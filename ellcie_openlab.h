/*
  SSIManager.h - Library for computing and checking crc16 + generating ssi commands.
  Created by Pierre LeCoz and Alexandre Howard.
  7/25/2019
*/
#include <stddef.h>
#include <stdint.h>
#include <stdlib.h>

#define bool uint8_t

#define NUMLINE 8
#define NUMCOL 8
#define FRAMETOSKIP 10

#define SSI_FRAME_MAX_PAYLOAD_SIZE 255
#define SSI_FRAME_HEADER_SIZE 3
#define SSI_FRAME_FOOTER_SIZE 2
#define SSI_FRAME_OVERHEAD_SIZE (SSI_FRAME_HEADER_SIZE + SSI_FRAME_FOOTER_SIZE)
#define SSI_FRAME_MAX_LENGTH (SSI_FRAME_MAX_PAYLOAD_SIZE + SSI_FRAME_OVERHEAD_SIZE)

#define SSI_FRAME_SOF 0xFE
#define SSI_FRAME_SOF_INDEX 0
#define SSI_FRAME_LEN_INDEX 1
#define SSI_FRAME_NOT_LEN_INDEX 2
#define SSI_FRAME_ADDR_INDEX 3
#define SSI_FRAME_CMD_INDEX 4
#define SSI_FRAME_SOP_INDEX 5

#define OPENLAB_MAX_SENSOR_DATA 16

#define OPENLAB_V3_LED_BLUE	5
#define OPENLAB_V3_LED_GREEN	6
#define OPENLAB_V3_LED_RED	9

enum ssi_commands_defition
{
    SSI_WILDCARD = '?',
    SSI_QUERY = 'q',
    SSI_QUERY_RSP = 'a',
    SSI_DISCOVER_SENSORS = 'c',
    SSI_DISCOVER_REPLY = 'n',
    SSI_GET_CONFIGURATION_DATA = 'g',
    SSI_CONFIGURATION_DATA_RSP = 'x',
    SSI_SENSOR_DATA_RSP = 'd',
    SSI_SENSOR_MANY_DATA_RSP = 'm',
    SSI_SENSOR_MANY_DATA_RSP_NO_CRC = 'M',
    SSI_CREATE_SENSOR_OBSERVER = 'o',
    SSI_OBSERVER_CREATED = 'y',
    SSI_KILL_SENSOR_OSERVER = 'k',
    SSI_OBSERVER_FINISHED = 'u',
    SSI_FREE_DATA = 'f'
};

typedef struct ssi_command_s
{
    uint8_t addr;
    uint8_t cmd;
    uint8_t data[SSI_FRAME_MAX_PAYLOAD_SIZE - 2 /* addr and cmd are part of the payload */];
    uint8_t payload_size;
} __attribute__((__packed__)) ssi_command_t;

typedef struct ssi_query_response_s
{
    uint16_t version;
    uint16_t buffer_size;
    uint16_t delay;
    uint16_t reserve_2;
} __attribute__((__packed__)) ssi_query_response_t;

enum ssi_type
{
    SSI_TYPE_FLOAT = 0x00,
    SSI_TYPE_S32 = 0x01,
    SSI_TYPE_OTHER = 0x02,
    SSI_TYPE_U8 = 0x03,
    SSI_END_DISCOVERY = 0xFFFF
};

typedef struct ssi_discover_reply_s
{
    uint16_t sensor_id;
    char sensor_desc[16];
    char unit[8];
    uint8_t type;
    int8_t scaler;
    uint32_t min;
    uint32_t max;
} __attribute__((__packed__)) ssi_discover_reply_t;

typedef struct ssi_reset_sensor_device_s
{

} __attribute__((__packed__)) ssi_reset_sensor_device_t;

typedef struct ssi_configuration_data_response_s
{
    uint16_t sensor_id;
    uint8_t type_attrib : 4;
    uint8_t type_data : 4;
    char attrib[32];
    char value[32];

} __attribute__((__packed__)) ssi_configuration_data_response_t;

typedef struct ssi_sensor_data_response_M_s
{
    uint16_t sensor_id;
    union {
        uint8_t u8_tab[OPENLAB_MAX_SENSOR_DATA];
        uint32_t u32_tab[4];
    } type;
} __attribute__((__packed__)) ssi_sensor_data_response_M_t;

typedef struct ssi_create_sensor_observer_s
{
    uint16_t interval;
    uint8_t mult;
    uint8_t count;
    uint8_t len;
    uint32_t threshold;
    uint16_t sensor_id1;
} __attribute__((__packed__)) ssi_create_sensor_observer_t;

typedef struct ssi_observer_created_s
{
    uint8_t id;
} __attribute__((__packed__)) ssi_observer_created_t;

typedef struct ssi_observer_finished_s
{
    uint8_t id;
} __attribute__((__packed__)) ssi_observer_finished_t;

typedef struct ssi_free_data_s
{
    uint8_t data;
} __attribute__((__packed__)) ssi_free_data_t;

void ssi_init(uint8_t device_addr, uint8_t sensor_id, char *sensor_desc, char *unit, enum ssi_type type);
uint16_t ssi_fnCRC16(const uint8_t *ptrFrame, uint16_t len);
int ssi_frame_parser(char c, void *buffer, uint16_t sz);
void ssi_command_handler(ssi_command_t *cmd);
void ssi_read_serial_input(void);
uint8_t ssi_read_free_data(void);
bool ssi_is_observer_created(void);
bool ssi_is_inited(void);
