# ellcie-openlab-library

This code contains the arduino OpenLab library. 
It can be used in any project to interact with Ellcie Antilope glasses.


OpenLAB V3 (ATmega328P): 

1 Mhz (using Internal 8 Mhz)

```cmd
avrdude -c avrispmkii -p atmega328p -U lfuse:w:0x62:m -U hfuse:w:0xDB:m -U efuse:w:0xFF:m
```

2 Mhz (using External 16 Mhz)

```cmd
avrdude -c avrispmkii -p atmega328p -U lfuse:w:0x7F:m -U hfuse:w:0xDB:m -U efuse:w:0xFF:m
```

8 Mhz (using Internal 8 Mhz)

```cmd
avrdude -c avrispmkii -p atmega328p -U lfuse:w:0xE2:m -U hfuse:w:0xDB:m -U efuse:w:0xFF:m
```

16 Mhz (using External 16 Mhz) 

```cmd
avrdude -c avrispmkii -p atmega328p -U lfuse:w:0xFF:m -U hfuse:w:0xDB:m -U efuse:w:0xFF:m 
```

Before flashing a program to 1 and 2 Mhz boards, new boards (Arduino Pro or Pro Mini (1.8V, 1 MHz) and (1.8V, 2 MHz)) must be added to the arduino IDE. 
This is done by adding to arduino_folder/hardware/arduino/avr/boards.txt the following lines : 

```cmd
pro.menu.cpu.1MHzatmega328p.upload=avrdude
pro.menu.cpu.1MHzatmega328p=ATmega368p (1.8V, 1 MHz)
pro.menu.cpu.1MHzatmega328p.build.mcu=atmega328p
pro.menu.cpu.1MHzatmega328p.build.f_cpu=1000000L

pro.menu.cpu.2MHzatmega328p.upload=avrdude
pro.menu.cpu.2MHzatmega328p=ATmega368p (1.8V, 2 MHz)
pro.menu.cpu.2MHzatmega328p.build.mcu=atmega328p
pro.menu.cpu.2MHzatmega328p.build.f_cpu=2000000L
```

If AVRisp is too fast to burn 1Mhz MCUs, reduce speed by addind "-B5" option (in arduino_folder/hardware/arduino/avr/platform.txt) as follows:

```cmd
tools.avrdude.program.pattern="{cmd.path}" "-C{config.path}" {program.verbose} {program.verify} -p{build.mcu} -c{protocol} -B5 {program.extra_params} "-Uflash:w:{build.path}/{build.project_name}.hex:i"
``` 

 
OpenLAB V1 (ATmega2560): 

Read fuses : 
avrdude -c usbasp -p m2560 -U lfuse:r:lowfuseval.hex:h -U hfuse:r:highfuseval.hex:h 

Write fuses (arduino mega config)
avrdude -c usbasp -p m2560 -U lfuse:w:0xFF:m -U hfuse:w:0xD8:m -U efuse:w:0xFD:m 

Read flash : 
avrdude -c usbasp -p m2560 -U flash:r:bootloader+blink.hex:i

Write flash :
avrdude -c usbasp -p m2560 -U flash:w:file.hex


