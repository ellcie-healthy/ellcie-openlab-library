/*
  SSIManager.cpp - Library for computing and checking crc16 + generating ssi commands.
  Created by Pierre LeCoz and Alexandre Howard.
  7/25/2019
*/

#include "ellcie_openlab.h"
#include <HardwareSerial.h>
#include <string.h>

#if defined(__AVR_ATmega1280__) || defined(__AVR_ATmega2560__)
#define DEBUG_ON
#define Serial_glasses Serial1
#define Serial_debug Serial
#else
#define Serial_glasses Serial
#endif

static uint8_t g_frame_buffer[SSI_FRAME_MAX_LENGTH];

static bool init_done = false;
static uint8_t g_device_addr;
static uint8_t g_sensor_id;
static bool g_observer_created = false;
volatile static uint8_t g_free_data = 0;

void send_ssi_cmd(const ssi_command_t *cmd)
{
    uint16_t calc_crc = 0;
    size_t payload_size = cmd->payload_size;
    size_t frame_len = SSI_FRAME_HEADER_SIZE + payload_size;
    bool crc_enabled = false;
    uint8_t ssi_cmd = cmd->cmd;

    // crc is enabled if command is written in lower case
    if ((ssi_cmd >= 'a') && (ssi_cmd <= 'z'))
    {
        crc_enabled = true;
        frame_len += SSI_FRAME_FOOTER_SIZE;
    }

    uint8_t frame[frame_len];
    size_t i = 0;

    // put start of frame byte
    frame[0] = SSI_FRAME_SOF;

    // set frame len
    frame[SSI_FRAME_LEN_INDEX] = frame_len;
    frame[SSI_FRAME_NOT_LEN_INDEX] = ~frame_len;

    // set payload
    memcpy(frame + SSI_FRAME_NOT_LEN_INDEX + 1, cmd, frame_len - SSI_FRAME_HEADER_SIZE);

    if (crc_enabled)
    {
        // calculate and set crc
        calc_crc = ssi_fnCRC16(frame, frame_len);

        frame[SSI_FRAME_HEADER_SIZE + payload_size] = (calc_crc >> 8) & 0xFF;
        frame[SSI_FRAME_HEADER_SIZE + payload_size + 1] = calc_crc & 0xFF;
    }

    for (i = 0; i < frame_len; i++)
    {
#ifdef DEBUG_ON
        //Serial_debug.println(frame[i], HEX);
#endif
        Serial_glasses.write(frame[i]);
    }
}

static ssi_command_t ssi_command;
static ssi_query_response_t ssi_query_response;
static ssi_discover_reply_t ssi_discover_reply;

static void send_ssi_cmd_query_response(void)
{
    ssi_command.addr = g_device_addr;
    ssi_command.cmd = SSI_QUERY_RSP;
    ssi_command.payload_size = 2 /* addr + cmd */ + sizeof(ssi_query_response_t);

    memcpy(ssi_command.data, &ssi_query_response, sizeof(ssi_query_response_t));

    send_ssi_cmd(&ssi_command);
}

static void send_ssi_send_discover_reply(void)
{
    ssi_command.addr = g_device_addr;
    ssi_command.cmd = SSI_DISCOVER_REPLY;
    ssi_command.payload_size = 2 /* addr + cmd */ + sizeof(ssi_discover_reply_t);

    memcpy(ssi_command.data, &ssi_discover_reply, sizeof(ssi_discover_reply_t));

    send_ssi_cmd(&ssi_command);
}

static void send_ssi_sensor_data_response_M(uint16_t sensor_id, uint8_t *data, uint8_t nb_data)
{
    static ssi_sensor_data_response_M_t ssi_sensor_data_response_M;

    ssi_command.addr = g_device_addr;
    ssi_command.cmd = SSI_SENSOR_MANY_DATA_RSP_NO_CRC;
    ssi_command.payload_size = 2 /* addr + cmd */ + sizeof(ssi_sensor_data_response_M_t);

    ssi_sensor_data_response_M.sensor_id = sensor_id;
    memcpy(ssi_sensor_data_response_M.type.u8_tab, data, nb_data);

    memcpy(ssi_command.data, &ssi_sensor_data_response_M, sizeof(ssi_sensor_data_response_M_t));

    send_ssi_cmd(&ssi_command);
}

static void send_ssi_observer_created(void)
{
    ssi_command_t ssi_command;
    ssi_observer_created_t ssi_observer_created;

    ssi_command.addr = g_device_addr;
    ssi_command.cmd = SSI_OBSERVER_CREATED;
    ssi_command.payload_size = 2 /* addr + cmd */ + sizeof(ssi_observer_created_t);

    ssi_observer_created.id = g_sensor_id;

    memcpy(ssi_command.data, &ssi_observer_created, sizeof(ssi_observer_created_t));

    send_ssi_cmd(&ssi_command);
}

static void send_ssi_observer_finished(void)
{
    ssi_command_t ssi_command;
    ssi_observer_finished_t ssi_observer_finished;

    ssi_command.addr = g_device_addr;
    ssi_command.cmd = SSI_OBSERVER_FINISHED;
    ssi_command.payload_size = 2 /* addr + cmd */ + sizeof(ssi_observer_finished_t);

    ssi_observer_finished.id = g_sensor_id;

    memcpy(ssi_command.data, &ssi_observer_finished, sizeof(ssi_observer_finished_t));

    send_ssi_cmd(&ssi_command);
}

uint16_t ssi_fnCRC16(const uint8_t *ptrFrame, uint16_t len)
{
    // Shift SOF, len and ~len
    ptrFrame += SSI_FRAME_HEADER_SIZE;

    uint16_t crc = 0;
    uint16_t i, j = 0;

    for (i = 0; i < (len - SSI_FRAME_OVERHEAD_SIZE); i++)
    {
        crc ^= *ptrFrame++;

        for (j = 0; j < 8; j++)
        {
            if (crc & 1)
            {
                crc = crc >> 1;
                crc ^= 0xA001;
            }
            else
            {
                crc = crc >> 1;
            }
        }
    }

    return crc;
}

int ssi_frame_check_crc(uint8_t *frame, uint16_t len)
{
    uint8_t payload_size = 0;
    uint8_t cmd = 0;
    int8_t crc_pos = 0;
    uint16_t lo = 0;
    uint16_t hi = 0;
    uint16_t rcv_crc = 0, calc_crc = 0;
    int ret = -1;

    if (!frame)
    {
        return -1;
    }

    if (len <= SSI_FRAME_NOT_LEN_INDEX)
    {
        return -1;
    }

    cmd = frame[SSI_FRAME_CMD_INDEX];

    if (cmd > 'A' && cmd < 'Z')
    {
        // No CRC when command is upper case
        return 0;
    }

    payload_size = frame[SSI_FRAME_LEN_INDEX] - SSI_FRAME_OVERHEAD_SIZE;
    crc_pos = SSI_FRAME_HEADER_SIZE + payload_size;

    if ((crc_pos + 1) >= frame[SSI_FRAME_LEN_INDEX])
    {
        return -1;
    }

    hi = (((uint16_t)frame[crc_pos]) << 8) & 0xFF00;
    lo = (uint16_t)frame[crc_pos + 1] & 0x00FF;

    // Read CRC
    rcv_crc = (hi | lo);
    // Is CRC ok ?
    // Re-Calculate CRC
    calc_crc = ssi_fnCRC16((uint8_t *)frame, (uint16_t)len);

    if (calc_crc - rcv_crc == 0)
    {
        // crc ok
        ret = 0;
    }

    return ret;
}

void ssi_command_handler(ssi_command_t *cmd)
{
#ifdef DEBUG_ON
    Serial_debug.print("ssi cmd rcv = ");
    Serial_debug.println((char)cmd->cmd);
#endif
    ssi_free_data_t ssi_free_data;

    switch (cmd->cmd)
    {
        case SSI_QUERY:
            send_ssi_cmd_query_response();
            init_done = false;
            g_observer_created = false;
            break;

        case SSI_DISCOVER_SENSORS:
            send_ssi_send_discover_reply();
            init_done = true;
            break;

        case SSI_CREATE_SENSOR_OBSERVER:
            g_observer_created = true;
            send_ssi_observer_created();
            break;

        case SSI_KILL_SENSOR_OSERVER:
            g_observer_created = false;
            send_ssi_observer_finished();
            break;

        case SSI_FREE_DATA:
            memcpy(&ssi_free_data, cmd->data, sizeof(ssi_free_data_t));
            g_free_data = ssi_free_data.data;
#ifdef DEBUG_ON
            Serial_debug.print("free_data = ");
            Serial_debug.println(g_free_data, DEC);
#endif
            break;

        default:
            break;
    }
}

bool ssi_is_observer_created(void)
{
    return g_observer_created;
}

int ssi_frame_parser(uint8_t c, void *buffer, uint16_t sz)
{
    static uint16_t g_char_count = 0;
    static uint8_t frameDetected = 0;
    static uint16_t size = 0;

    if (c == SSI_FRAME_SOF)
    {
        frameDetected = 1;
    }

    if (frameDetected)
    {
        g_frame_buffer[g_char_count] = c;
        g_char_count++;
    }

    if (g_char_count == SSI_FRAME_LEN_INDEX + 1)
    {
        size = (uint8_t)g_frame_buffer[SSI_FRAME_LEN_INDEX];
    }

    if ((size > 0) && (g_char_count == size) && frameDetected)
    {
        frameDetected = 0;
        g_char_count = 0;

        if (ssi_frame_check_crc(g_frame_buffer, size) == 0)
        {
            if (sz >= (uint8_t)g_frame_buffer[SSI_FRAME_LEN_INDEX])
            {
                memcpy(buffer, g_frame_buffer + SSI_FRAME_HEADER_SIZE, (g_frame_buffer[SSI_FRAME_LEN_INDEX] - SSI_FRAME_HEADER_SIZE) * sizeof(char));
                return (uint8_t)g_frame_buffer[SSI_FRAME_LEN_INDEX];
            }
        }
        else
        {
            return -1;
        }
    }
    else if ((size > 0) && (g_char_count > size) && frameDetected)
    {
        frameDetected = 0;
        g_char_count = 0;
    }

    return 0;
}

void ssi_read_serial_input(void)
{
    static ssi_command_t ssi_cmd;
    uint8_t c = 0;
    int sz = 0;

    while (Serial_glasses.available() > 0)
    {
        c = Serial_glasses.read();
#ifdef DEBUG_ON
        Serial_debug.print(c, HEX);
#endif
        if ((sz = ssi_frame_parser(c, (void *)&ssi_cmd, sizeof(ssi_command_t))) >= SSI_FRAME_CMD_INDEX)
        {
            ssi_command_handler(&ssi_cmd);
        }
    }
}

uint8_t ssi_read_free_data(void)
{
    uint8_t free_data_tmp = g_free_data;
    g_free_data = 0;
    return free_data_tmp;
}

void ssi_init(uint8_t device_addr, uint8_t sensor_id, char *sensor_desc, char *unit, enum ssi_type type)
{
#ifdef DEBUG_ON
    Serial_debug.println("SSI init device");
#endif
    g_device_addr = device_addr;
    g_sensor_id = sensor_id;

    // SSI_QUERY_RSP
    ssi_query_response.version = 0x0002;
    ssi_query_response.buffer_size = 0x00FF;
    ssi_query_response.delay = 0x0000;
    ssi_query_response.reserve_2 = 0x0000;

    //SSI_DISCOVER_REPLY
    ssi_discover_reply.sensor_id = g_sensor_id;
    memset(ssi_discover_reply.sensor_desc, 0, sizeof(ssi_discover_reply.sensor_desc));
    memcpy(ssi_discover_reply.sensor_desc, sensor_desc, strlen(sensor_desc));
    memset(ssi_discover_reply.unit, 0, sizeof(ssi_discover_reply.unit));
    memcpy(ssi_discover_reply.unit, unit, strlen(unit));
    ssi_discover_reply.type = type;
    ssi_discover_reply.scaler = 1;
    ssi_discover_reply.min = 0;
    ssi_discover_reply.max = 255;
}

bool ssi_is_inited()
{
    return init_done;
}
